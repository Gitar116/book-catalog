package ru.itpark.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
//@NoArgsConstructor
public class Book {
    @Id
    @GeneratedValue
    private int id;
    private String name;
    private String publishing;
    private String author;
    private String room;
    private int shelf;
    private boolean reserve;

    public Book() {
    }

    public Book(String name, String publishing, String author, String room, int shelf, boolean reserve) {
        this.name = name;
        this.publishing = publishing;
        this.author = author;
        this.room = room;
        this.shelf = shelf;
        this.reserve = reserve;
    }
}
