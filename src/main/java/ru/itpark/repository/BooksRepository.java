package ru.itpark.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itpark.entity.Book;

import java.util.List;

@Repository
public interface BooksRepository extends JpaRepository<Book, Integer> {

    List<Book> findAllByNameContains (String name);

    List<Book> findAllByPublishingContains (String publishing);

    List<Book> findAllByAuthorContains (String Author);

    List<Book> findAllByReserveContains(Boolean reserved);
}
