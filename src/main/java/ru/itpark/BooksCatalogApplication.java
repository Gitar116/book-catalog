package ru.itpark;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.itpark.controller.BooksController;
import ru.itpark.entity.Account;
import ru.itpark.entity.Book;
import ru.itpark.repository.AccountRepository;
import ru.itpark.repository.BooksRepository;

import java.util.List;

@SpringBootApplication
public class BooksCatalogApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(BooksCatalogApplication.class, args);
        BooksRepository repository = context.getBean(BooksRepository.class);

        Book first = new Book("Name", "Publishing", "Author", "Room", 1, false);
        repository.save(first);


        var encoder = context.getBean(PasswordEncoder.class);
        var repository2 = context.getBean(AccountRepository.class);
        repository2.saveAll(
                List.of(
                        new Account(0,
                                "admin",
                                encoder.encode("admin"),
                                List.of(new SimpleGrantedAuthority("ROLE_ADMIN")),
                                true,
                                true,
                                true,
                                true
                        ),
                        new Account(
                                0,
                                "user",
                                encoder.encode("user"),
                                List.of(new SimpleGrantedAuthority("ROLE_USER")),
                                true,
                                true,
                                true,
                                true
                        )
                )
        );
    }
}
