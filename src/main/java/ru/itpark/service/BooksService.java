package ru.itpark.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;
import ru.itpark.entity.Book;

public interface BooksService {
    List<Book> findAll();

    void add(Book book, MultipartFile image);

    void removeById(int id);

    List<Book> findByName(String name);
    List<Book> findByPublishing(String publishing);
    List<Book> findByAuthor(String author);
    List<Book> findByReserve(Boolean reserved);

    Book findById(int id);
}

