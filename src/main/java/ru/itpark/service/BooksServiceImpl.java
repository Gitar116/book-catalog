package ru.itpark.service;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.itpark.entity.Book;
import ru.itpark.repository.BooksRepository;


@Service
public class BooksServiceImpl implements BooksService {
    private final BooksRepository booksRepository;
    private final Environment environment;

    @Autowired
    public BooksServiceImpl(BooksRepository booksRepository, Environment environment) {
        this.booksRepository = booksRepository;
        this.environment = environment;
    }

    public List<Book> findAll() {
        return booksRepository.findAll();
    }

    public void add(Book book, MultipartFile image) {
        Book saved = booksRepository.save(book);

        Path path = Paths.get(
                environment.getProperty("user.dir"),
                "upload",
                "static"
        );
        try {
            image.transferTo(
                    path.resolve(saved.getId() + ".jpg").toFile()
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Book> findByName(String name) {
        return booksRepository.findAllByNameContains(name);
    }

    @Override
    public List<Book> findByPublishing(String publishing) {
        return booksRepository.findAllByPublishingContains(publishing);
    }

    @Override
    public List<Book> findByAuthor(String author) {
        return booksRepository.findAllByAuthorContains(author);
    }

    @Override
    public List<Book> findByReserve(Boolean reserved) {
        return booksRepository.findAllByReserveContains(reserved);
    }

    @Override
    public Book findById(int id) {
        return booksRepository.getOne(id);
    }

    @Override
    public void removeById(int id) {
        booksRepository.deleteById(id);
    }
}
