package ru.itpark.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.itpark.entity.Account;
import ru.itpark.entity.Book;
import ru.itpark.service.BooksService;

@Controller
@RequestMapping("/booksList")
public class BooksController {
    private final BooksService booksService;

    public BooksController(BooksService booksService) {
        this.booksService = booksService;
    }

    @GetMapping
    public String getAll(Model model, @AuthenticationPrincipal Account account) {
        model.addAttribute("booksList", booksService.findAll());
        model.addAttribute("account", account);
        return "pages/booksList";
    }

    @GetMapping("/add")
    public String addForm(Model model) {
        model.addAttribute("booksList", booksService.findAll());
        return "/pages/book-add";
    }

    @PostMapping("/add")
    public String add(@ModelAttribute Book book, @RequestParam MultipartFile image) {
        booksService.add(book, image);
        return "redirect:/booksList";
    }

    @GetMapping("/{id}")
    public String get(@PathVariable int id, Model model) {
        model.addAttribute("book", booksService.findById(id));
        return "pages/book";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/{id}/remove")
    public String remove(@PathVariable int id) {
        booksService.removeById(id);
        return "redirect:/booksList";
    }

    @GetMapping("/{name}")
    public String findByName(@PathVariable String name, Model model) {
        model.addAttribute("books-search", booksService.findByName(name));
        return "redirect:/books-search";
    }

    @GetMapping("/{publishing}")
    public String findByPublishing(@PathVariable String publishing, Model model) {
        model.addAttribute("books-search", booksService.findByPublishing(publishing));
        return "redirect:/books-search";
    }

    @GetMapping("/{author}")
    public String findByAuthor(@PathVariable String author, Model model) {
        model.addAttribute("books-search", booksService.findByAuthor(author));
        return "redirect:/books-search";
    }

    @GetMapping("/addSearch")
    public String addFormSearch(Model model) {
        model.addAttribute("books-search", booksService.findAll());
        return "/pages/books-search";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/addReport")
    public String addFormReport(Model model) {
        model.addAttribute("books-report", booksService.findAll());
        return "/pages/books-report";
    }

    @GetMapping("/{reserved}")
    public String Report(@PathVariable boolean reserved, Model model) {
        model.addAttribute("books-report", booksService.findByReserve(reserved));
        return "redirect:/books-report";
    }

}